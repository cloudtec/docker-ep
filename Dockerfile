FROM php:7.2-fpm-alpine3.12

### ENVIRONMENT VARIABLES ##############################################################################################

ENV MAINTENANCE_FLAG=/home/default/.maintenance-mode

ENV TIMEZONE="Europe/Zurich"

ENV UID=1000
ENV GID=1000

ENV HISTCONTROL=ignoredups

ENV NGINX_WEBROOT=/usr/share/nginx/html
ENV COMPOSER_HOME=/home/default/.cache/composer
ENV YARN_HOME=/home/default/.cache/yarn

# allow custom paths for all scripts
ENV HOOK_MAINTENANCE_ON="/scripts/maintenance-on.sh"
ENV HOOK_MAINTENANCE_OFF="/scripts/maintenance-off.sh"
ENV HOOK_HEALTHCHECK="/scripts/hooks/healthcheck.sh"
ENV HOOK_ENTRYPOINT="/scripts/hooks/entrypoint.sh"

# iconv charset issue (see "install packages" below)
# https://github.com/docker-library/php/issues/240
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

### DOCKER SETTINGS ####################################################################################################

WORKDIR ${NGINX_WEBROOT}
STOPSIGNAL SIGTERM

EXPOSE 80/tcp
EXPOSE 443/tcp

ENTRYPOINT ${HOOK_ENTRYPOINT}

### FOLDERS + FILES ####################################################################################################

ADD scripts/healthcheck/* /scripts/healthcheck/
ADD scripts/hooks/* /scripts/hooks/
ADD scripts/startup/* /scripts/startup/
ADD scripts/*.sh /scripts/
ADD bin/* /usr/local/bin/
ADD conf/nginx-include/* /etc/nginx/include/
ADD maintenance-page/* /usr/share/nginx/maintenance-page/

COPY conf/php-fpm /etc/init.d/php-fpm
COPY conf/php.ini /usr/local/etc/php/php.ini
COPY conf/php-cli.ini /usr/local/etc/php/php-cli.ini
COPY conf/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY conf/nginx.conf /etc/nginx/
COPY conf/nginx-application.conf /etc/nginx/sites-available/
COPY conf/nginx-maintenance.conf /etc/nginx/sites-available/
COPY conf/nginx-status.conf /etc/nginx/sites-available/
COPY conf/index.php /usr/share/nginx/html/public/
COPY conf/repositories /etc/apk/
COPY conf/docker-php-ext-apcu.ini /tmp/

RUN chmod -R 0755 /scripts \
  && chmod 0755 /usr/local/bin/* \
  && chown -R $UID:$GID /scripts \
  \
  \
### INSTALL PACKAGES ###################################################################################################
  && apk add --update --no-cache \
     bash git openssh rsync tzdata shadow fcgi bind-tools libcap \
     imagemagick autoconf freetype libpng libxml2 libssh2 libjpeg-turbo icu yaml zlib \
     nginx nginx-mod-http-headers-more rabbitmq-c-dev yarn ffmpeg \
  \
  # allow nginx to bind to ports <= 1024 \
  && setcap 'CAP_NET_BIND_SERVICE=+eip' /usr/sbin/nginx \
  \
  # iconv charset issue (see ENV above)
  && apk add --no-cache --repository https://dl-3.alpinelinux.org/alpine/edge/testing gnu-libiconv \
  \
  # run composer self-update
  && composer self-update \
### EXTEND PHP7.2 ###################################################################################################### \
  && mkdir /run/php-fpm \
  && chown $UID:$GID /run/php-fpm \
  && docker-php-source extract \
  # create /bin/php symlink
  && ln -fs /usr/local/bin/php /bin/php \
  # create symlinks for ffmpeg to cover a default setting
  && ln -fs /usr/bin/ffmpeg /usr/local/bin/ffmpeg \
  && ln -fs /usr/bin/ffprobe /usr/local/bin/ffprobe \
  # temporary packages
  && apk add --update --no-cache --virtual .build-deps zlib-dev icu-dev yaml-dev freetype-dev \
     gcc g++ libtool make \
     libpng-dev curl-dev libjpeg-turbo-dev imagemagick-dev libxml2-dev cyrus-sasl-dev libssh2-dev \
  # pecl extensions
  && pecl update-channels \
  && pecl install ssh2-1.3.1 \
  && docker-php-ext-enable ssh2 \
  && docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
  && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
  && docker-php-ext-install -j${NPROC} gd \
  && pecl install apcu \
  && docker-php-ext-enable apcu \
  && mv /tmp/docker-php-ext-apcu.ini /usr/local/etc/php/conf.d/ \
  && chown $UID:$GID /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini \
  && pecl install yaml \
  && docker-php-ext-enable yaml \
  && pecl install -o -f amqp \
  && docker-php-ext-enable amqp \
  # out-of-the-box extensions
  && docker-php-ext-install exif \
  && docker-php-ext-enable exif \
  && pecl install imagick-3.4.4 \
  && docker-php-ext-enable imagick \
  && docker-php-ext-install bcmath \
  && docker-php-ext-install calendar \
  && docker-php-ext-install iconv \
  && docker-php-ext-install hash \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl \
  && docker-php-ext-install json \
  && docker-php-ext-install mbstring \
  && docker-php-ext-install opcache \
  && docker-php-ext-install pdo \
  && docker-php-ext-install pdo_mysql \
  && docker-php-ext-install simplexml \
  && docker-php-ext-install soap \
  && docker-php-ext-install sockets \
  && docker-php-ext-install tokenizer \
  && docker-php-ext-install xml \
  && docker-php-ext-install zip \
  && docker-php-ext-install mysqli \
  # install php-http extension (depends on curl-dev, iconv, and the things listed here ->)
  && pecl install raphf propro \
  && docker-php-ext-enable raphf propro \
  && pecl install pecl_http-3.2.4 \
  && echo -e "extension=raphf.so\nextension=propro.so\nextension=http.so" > /usr/local/etc/php/conf.d/docker-php-ext-http.ini \
  && rm -rf /usr/local/etc/php/conf.d/docker-php-ext-raphf.ini \
  && rm -rf /usr/local/etc/php/conf.d/docker-php-ext-propro.ini \
  # cleanup
  && rm -rf /usr/local/lib/php/doc/* \
  && rm -rf /tmp/* \
  && docker-php-source delete \
  && apk del .build-deps \
  \
  \
##### SETUP NGINX ######################################################################################################
  && mkdir /etc/nginx/sites-enabled \
  && ln -fs /etc/nginx/sites-available/nginx-application.conf /etc/nginx/sites-enabled/ \
  && ln -fs /etc/nginx/sites-available/nginx-status.conf /etc/nginx/sites-enabled/ \
  && mkdir /run/nginx \
  && chown $UID:$GID /run/nginx \
  && chmod 0755 /run/nginx \
  && mkdir -p /var/lib/nginx/tmp \
  && chown -R $UID:$GID /var/lib/nginx \
  && chmod 0777 /var/lib/nginx/tmp \
  && chown -R $UID:$GID /etc/nginx \
  \
  \
###### Add container user (modified in entrypoint.sh) ##################################################################
  && addgroup --gid "${GID}" default \
  && adduser -D -g "" -s /bin/bash -G default -u ${UID} -h /home/default default \
  && chown -R $UID:$GID /home/default \
  \
  \
##### SETUP DEFAULT FOLDERS ############################################################################################
  # composer cache folder
  && mkdir -p $COMPOSER_HOME \
  && chown -R default:default $COMPOSER_HOME \
  && chmod -R 0755 $COMPOSER_HOME \
  # yarn cache folder
  && mkdir -p $YARN_HOME \
  && chown -R default:default $YARN_HOME \
  && chmod -R 0755 $YARN_HOME \
  # nginx log folder
  && mkdir -p /var/log/nginx \
  && chown default:default /var/log/nginx \
  && chmod 0755 /var/log/nginx \
  # php-fpm log folder
  && mkdir /var/log/php-fpm \
  && chown default:default /var/log/php-fpm \
  && chmod 0755 /var/log/php-fpm \
  # webroot
  && chown -R default:default $NGINX_WEBROOT

# set default uid/gid for container entrypoint and shell
USER $UID:$GID

# declare external volumes (composer cache and webroot)
VOLUME $NGINX_WEBROOT
VOLUME $COMPOSER_HOME
VOLUME $YARN_HOME
