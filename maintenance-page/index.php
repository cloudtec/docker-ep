<?php

header('HTTP/1.0 503 Service Unavailable', true, 503);
header('Retry-After: 300');

$accept = (array_key_exists('HTTP_ACCEPT', $_SERVER) && strlen($_SERVER['HTTP_ACCEPT']) > 3)
    ? $_SERVER['HTTP_ACCEPT']
    : 'text/html';

// output text/html or application/json -->

if (strpos($accept, 'text/html') !== -1) {
    header('Content-Type: text/html', true);
    echo <<<MAINTENANCE_HTML
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>System Maintenance</title>
</head>
<body>
<h1>System Maintenance</h1>
<p>Please check later</p>
</body>
</html>
MAINTENANCE_HTML;

}
elseif (strpos($accept, 'application/json') !== -1) {
    header('Content-Type: application/json', true);
    echo <<<MAINTENANCE_JSON
{
    "error": {
        "code": 1234,
        "message": "System Maintenance"
    }
}
MAINTENANCE_JSON;

}
else {
    // empty body
    exit;
}
