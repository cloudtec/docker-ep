# Makefile assumes that all related images are available in the parent-directory
.PHONY: update-higher-in-hierarchy-ep update-docker-image
SHELL = /bin/bash

update-higher-in-hierarchy-ep:
	make update-docker-image
	cd ../docker-emp/ && git checkout master && make update-higher-in-hierarchy-emp

update-docker-image:
	@echo "---updating cloudtecbern/ep:v2"
	docker build --pull --no-cache --compress -t cloudtecbern/ep:v2 .
	docker push cloudtecbern/ep:v2
