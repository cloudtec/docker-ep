#!/bin/bash

# Don't run health check during maintenance.
if [[ -f ${MAINTENANCE_FLAG} ]]; then
    exit 0
fi

# Run health check on each registered background service, exit with code 1 if any check fails:
for healthcheck_script in /scripts/healthcheck/*; do
    source ${healthcheck_script} || exit 1
done