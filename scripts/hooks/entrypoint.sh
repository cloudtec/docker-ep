#!/bin/bash

# run entry point scripts for each registered background service:
for startup_script in /scripts/startup/*; do
    source ${startup_script}
done

# start NGINX in foreground
nginx -g "daemon off;"