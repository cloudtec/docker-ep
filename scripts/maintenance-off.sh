#!/bin/bash

# check UID (service restart requires root)
if [[ "$(id -u)" != "$UID" ]]; then
    echo "[ERROR] UID $UID required to run $HOOK_MAINTENANCE_OFF (Logged in as $(id -u))"
    return 1
fi

# switch NGINX vhost
rm /etc/nginx/sites-enabled/nginx-maintenance.conf
ln -fs /etc/nginx/sites-available/nginx-application.conf /etc/nginx/sites-enabled/

# restart services
/etc/init.d/php-fpm reload
nginx -s reload
sleep 2

# remove lock
if [[ -f $MAINTENANCE_FLAG ]]; then
  rm ${MAINTENANCE_FLAG}
fi