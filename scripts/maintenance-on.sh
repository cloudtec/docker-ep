#!/bin/bash

# check UID (service restart requires root)
if [[ "$(id -u)" != "$UID" ]]; then
    echo "[ERROR] UID $UID required to run $HOOK_MAINTENANCE_ON (Logged in as $(id -u))"
    return 1
fi

# add maint. lock
touch ${MAINTENANCE_FLAG}

# switch NGINX vhost
rm /etc/nginx/sites-enabled/nginx-application.conf
ln -fs /etc/nginx/sites-available/nginx-maintenance.conf /etc/nginx/sites-enabled/

# restart services
/etc/init.d/php-fpm reload
nginx -s reload
sleep 2