#!/bin/bash

#
# very basic check: return exit code 1 if no php-fpm process is running.
#

pidof php-fpm > /dev/null || return 1